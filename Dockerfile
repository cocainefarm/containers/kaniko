FROM gcr.io/kaniko-project/executor:latest as kaniko

FROM alpine:latest

ENV DOCKER_CONFIG='/kaniko/.docker'

RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
        gettext \
        git \
        jq

COPY --from=kaniko /kaniko/executor /kaniko/
COPY --from=kaniko /kaniko/docker-credential-ecr-login /kaniko/
COPY --from=kaniko /kaniko/.docker/config.json /kaniko/.docker/
ENV PATH=/kaniko:$PATH

